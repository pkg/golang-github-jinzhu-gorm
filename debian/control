Source: golang-github-jinzhu-gorm
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Tianon Gravi <tianon@debian.org>,
           Michael Stapelberg <stapelberg@debian.org>,
           Nobuhiro Iwamatsu <iwamatsu@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-denisenkom-go-mssqldb-dev,
               golang-github-erikstmartin-go-testdb-dev,
               golang-github-go-sql-driver-mysql-dev,
               golang-github-jinzhu-inflection-dev,
               golang-github-jinzhu-now-dev,
               golang-github-lib-pq-dev,
               golang-github-mattn-go-sqlite3-dev,
               tzdata
Standards-Version: 4.6.2
Homepage: https://github.com/jinzhu/gorm
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-jinzhu-gorm
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-jinzhu-gorm.git
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-go
XS-Go-Import-Path: github.com/jinzhu/gorm

Package: golang-github-jinzhu-gorm-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-denisenkom-go-mssqldb-dev,
         golang-github-go-sql-driver-mysql-dev,
         golang-github-jinzhu-inflection-dev,
         golang-github-lib-pq-dev,
         golang-github-mattn-go-sqlite3-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Description: ORM library for Golang
 The fantastic ORM library for Golang, aims to be developer friendly.
 .
 Overview
 .
   * Full-Featured ORM (almost)
   * Chainable API
   * Auto Migrations
   * Relations (Has One, Has Many, Belongs To, Many To Many, Polymorphism)
   * Callbacks (Before/After Create/Save/Update/Delete/Find)
   * Preloading (eager loading)
   * Transactions
   * Embed Anonymous Struct
   * Soft Deletes
   * Customizable Logger
   * Iteration Support via Rows
   * Every feature comes with tests
   * Developer Friendly
 .
 go doc format documentation for this project can be viewed online without
 installing the package by using the GoDoc page at:
 http://godoc.org/github.com/jinzhu/gorm
